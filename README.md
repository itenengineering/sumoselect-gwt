# SumoSelect - GWT #

This is a GWT wrapper for SumoSelect jQuery component developed by Hemant Negi. Original sumoselect can be found [here](http://hemantnegi.github.io/jquery.sumoselect/).

Some minor modification of the original code is made in order to properly integrate component with GWT. There is incompatibility between jQuery and GWT library, which prevents onchange event firing from SumoSelect component (and possibly other jQuery components as well). Modification provides needed workaround for proper functioning of onchange event within GWT. More info can be found on the following link [https://github.com/jquery/jquery/issues/1783](https://github.com/jquery/jquery/issues/1783).

This software is released under Apache License Version 2.0 (see LICENCE file).
