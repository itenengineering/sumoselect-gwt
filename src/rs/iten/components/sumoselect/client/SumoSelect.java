/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.sumoselect.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import rs.iten.components.sumoselect.resources.Resources;
import rs.iten.components.sumoselect.resources.ScriptResource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.OptionElement;
import com.google.gwt.dom.client.SelectElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;

/**
 * <p>
 * {@link SumoSelect} is a class that represents SumoSelect widget.
 * </p>
 * 
 * @author Marko Nikolić
 *
 */
public class SumoSelect extends Widget {

	private static ScriptResource scriptResource = GWT.create(ScriptResource.class);
	private static boolean scriptInjected = false; 
	
	/**
	 * Set script resource that will be used instead of bundled script, e.g. different
	 * version of SumoSelect JavaScript.
	 * 
	 * This method can be used before first instance is made. First instance will inject
	 * script into page and no further injection will happen, thus making this call unusable.
	 * 
	 * @param resource
	 */
	public static void setScriptResource(ScriptResource resource) {
		scriptResource = resource;
	}

	/**
	 * If script is not injected, inject it into page.
	 */
	{
		if (!scriptInjected) {
			ScriptInjector.fromString(scriptResource.sumoSelectJs().getText()).setWindow(ScriptInjector.TOP_WINDOW).setRemoveTag(false).inject();
			scriptInjected = true;
		}
	}	
	
	/**
	 * Select element where SumoSelect will attach
	 */
	SelectElement elSelect;
	
	/**
	 * Map of SumoSelect item values and their position within combo box
	 */
	Map<String, Integer> valuePositions = new HashMap<String, Integer>();
	
	/**
	 * Options used for this SumoSelect instance
	 */
	SumoSelectOptions options = new Options();
	
	/**
	 * Creates a default SumoSelect widget. 
	 */
	public SumoSelect() {
		this(null);
	}

	/**
	 * Creates a SumoSelect widget with specified options.
	 *
	 * @param options options for sumoselect widget
	 */	
	public SumoSelect(SumoSelectOptions options) {
		this(options, (Resources) GWT.create(Resources.class));
	}

	/**
	 * Creates a SumoSelect widget with specified options and resources.
	 *
	 * @param options options for sumoselect widget
	 * @param resources resources to be used for widget.
	 */
	public SumoSelect(SumoSelectOptions options, Resources resources) {
		super();
		
		if (resources != null)
			resources.sumoSelectCss().ensureInjected();
		
		if (options != null) {
			this.options = options;
		}
		
		elSelect = Document.get().createSelectElement();
		elSelect.setMultiple(this.options.getMultiple());
		elSelect.setName(this.options.getName());
		elSelect.setId("sselect");
		
		createSumoSelect(elSelect, this.options.getNativeOptions());

		setElement(elSelect.getParentElement());
	}
	
	/**
	 * Creates SumoSelect elements and attached to the select element.
	 */
	private native void createSumoSelect(Element elSelect, NativeSumoSelectOptions options) /*-{
		$wnd.$(elSelect).SumoSelect(options);
	}-*/;

	/**
	 * Adds new item at the end of the list with given value. Item text will be the same as value.
	 * 
	 * @param value value to be added
	 */
	public void add(String value) {
		add(value, value);
	}
	
	/**
	 * Adds new item at the end of the list with given value and text.
	 * 
	 * @param value value to be added
	 * @param text item text
	 */
	public void add(String value, String text) {
		add(value, text, valuePositions.size());
	}

	/**
	 * Adds new item at the end of the list with given value and text.
	 * 
	 * @param value value to be added
	 * @param text item text
	 * @param position position where item should be added
	 */
	public void add(String value, String text, int position) {
		valuePositions.put(value, position);

		native_add(value, text, position);
	}
	
	/**
	 * Adds new item to SumoSelect  - native implementation.
	 * 
	 * @param value value to be added
	 * @param text item to be added
	 * @param position position where item should be added
	 */
	private native void native_add(String value, String text, int position) /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.add(value, text, position);
	}-*/;
	
	/**
	 * Removes all valuePositions from SumoSelect.
	 * Note that this is slow operation and should be avoided when performance is an issue.
	 */
	public void clear() {
		while(valuePositions.size()>0) {
			remove(0);
		}
	}

	/**
	 * Removes item at index i.
	 * Note that this is slow operation and should be avoided when performance is an issue.
	 * 
	 * @param i index of the item to be removed, starting from 0.
	 */
	public void remove(int i) {
		// Delete value from position i and update position of other values
		String valueToRemove = null;
		for(String value : valuePositions.keySet()) {
			int index = valuePositions.get(value); 
			if (index == i) {
				// Avoid concurrent modification exception, postpone removal of this value.
				valueToRemove = value;
			}
			if (index > i) {
				valuePositions.put(value, index-1);
			}
		}

		valuePositions.remove(valueToRemove);
		native_remove(i);
	}
	
	/**
	 * Removes item at index i - native implementation.
	 * 
	 * @param i index of the item to be removed, starting from 0.
	 */
	private native void native_remove(int i) /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.remove(i);
	}-*/;

	/**
	 * Selects item with given value
	 * 
	 * @param value value to be selected
	 */
	public void selectItem(String value) {
		Integer index = valuePositions.get(value);
		if (index != null)
			selectItem(index);
		else
			unSelectAll();
	}
	
	/**
	 * Selects item at index i.
	 * 
	 * @param i index of the item to select.
	 */
	public native void selectItem(int i) /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.selectItem(i);
	}-*/;

	/**
	 * Selects all items.
	 */
	public native void selectAllItems() /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.SelectAll(0);
	}-*/;
	
	/**
	 * UnSelects item at index i.
	 * 
	 * @param i index of the item to unselect.
	 */
	public native void unSelectItem(int i) /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.unSelectItem(i);
	}-*/;

	/**
	 * UnSelects all items.
	 */
	public native void unSelectAll() /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.unSelectAll(0);		
	}-*/;
	
	/**
	 * Gets the index of currently-selected item. If multiple items are selected, this
	 * method will return index of the first selected item ({@link #getSelectedItems()}
	 * can be used to return array of all selected items).
	 * 
	 * @return the selected index, or <code>-1</code> if none is selected
	 */
	public int getSelectedItem() {
		NodeList<OptionElement> nodeList = elSelect.getOptions();
		for(int i=0; i<nodeList.getLength(); i++) {
			if (nodeList.getItem(i).isSelected())
				return i;
		}
		return -1;
	}

	/**
	 * Gets indices of all selected items.
	 * 
	 * @return list of selected indices.
	 */
	public ArrayList<Integer> getSelectedItems() {
		NodeList<OptionElement> nodeList = elSelect.getOptions();
		ArrayList<Integer> selectedItems = new ArrayList<Integer>();
		
		for(int i=0; i<nodeList.getLength(); i++) {
			if (nodeList.getItem(i).isSelected())
				selectedItems.add(i);
		}
		
		return selectedItems;
	}

	/**
	 * Returns currently selected value or null if no value is selected. If multiple
	 * values are selected, this method will return the first one selected {@link #getValues()}
	 * can be used to return array of all selected values).
	 * 
	 * @return currently selected value or null
	 */
	public String getValue() {
		int selectedIndex = getSelectedItem();
		
		if (selectedIndex != -1)
			return getValue(selectedIndex);
		else
			return null;
	}
	
	/**
	 * Returns value associated to given index.
	 * 
	 * @param i index of the value to return.
	 * @return value associated to given index.
	 */
	public String getValue(int i) {
		return elSelect.getOptions().getItem(i).getValue();
	}

	/**
	 * Gets values of all selected items.
	 * 
	 * @return list of selected values.
	 */
	public ArrayList<String> getValues() {
		NodeList<OptionElement> nodeList = elSelect.getOptions();
		ArrayList<String> selectedItems = new ArrayList<String>();
		
		for(int i=0; i<nodeList.getLength(); i++) {
			if (nodeList.getItem(i).isSelected())
				selectedItems.add(nodeList.getItem(i).getValue());
		}
		
		return selectedItems;
	}
	
	/**
	 * Enables item at index i.
	 * 
	 * @param i index of the item to enable.
	 */
	public void enableItem(int i) {
		native_enableItem(i);
	}
	
	/**
	 * Enables item at index i - native implementation.
	 * 
	 * @param i index of the item to enable.
	 */
	private native void native_enableItem(int i) /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.enableItem(i);
	}-*/;

	/**
	 * Disables control.
	 * 
	 * @param state true to disable control, false to enable.
	 */
	public void disable(boolean state) {
		native_disable(state);
	}
	
	/**
	 * Disables control - native implementation.
	 * 
	 * @param disabled true to disable control, false to enable.
	 */
	private native void native_disable(boolean state) /*-{
		var elSelect = this.@rs.iten.components.sumoselect.client.SumoSelect::elSelect;
		$wnd.$(elSelect)[0].sumo.disabled = state;
	}-*/;
	
	
	/**
	 * Adds handler that will be called when change event occurs.
	 * 
	 * @param handler handler to be called
	 * @return handler registration object
	 */
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return addDomHandler(handler, ChangeEvent.getType());
	}

	/**
	 * Adds handler that will be called when click event occurs.
	 * 
	 * @param handler handler to be called
	 * @return handler registration object
	 */
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return addDomHandler(handler, ClickEvent.getType());
	}

}
