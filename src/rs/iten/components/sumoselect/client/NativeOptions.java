/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.sumoselect.client;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * Options that can be set for a SumoSelect widget
 * 
 * @author Marko Nikolić
 */
public class NativeOptions extends JavaScriptObject implements NativeSumoSelectOptions {
	
	public final static native NativeSumoSelectOptions create()/*-{
		return {};
	}-*/;
	
	protected NativeOptions() {
	}
	
	@Override
	public final native void setPlaceholder(String placeholder) /*-{
		this.placeholder = placeholder;
	}-*/;
	
	@Override
	public final native String getPlaceholder() /*-{
		return this.placeholder;
	}-*/;

	@Override
	public final native void setCsvDispCount(int count) /*-{
		this.csvDispCount = count;
	}-*/;

	@Override
	public final native void setCaptionFormat(String format) /*-{
		this.captionFormat = format;
	}-*/;

	@Override
	public final native void setFloatWidth(int width) /*-{
		this.floatWidth = width; 
	}-*/;

	@Override
	public final native void setForceCustomRendering(boolean force) /*-{
		this.forceCustomRendering = force;
	}-*/;

	@Override
	public final native void setNativeOnDevice(String[] useragents) /*-{
		this.nativeOnDevice = useragents;
	}-*/;

	@Override
	public final native void setOutputAsCSV(boolean asCSV) /*-{
		this.outputAsCSV = asCSV;
	}-*/;

	@Override
	public final native boolean getOutputAsCSV() /*-{
		return this.outputAsCSV;
	}-*/;
	
	@Override
	public final native void setCsvSepChar(String separator) /*-{
		this.csvSepChar = separator;
	}-*/;

	@Override
	public final native void setOkCancelInMulti(boolean okCancel) /*-{
		this.okCancelInMulti = okCancel;
	}-*/;

	@Override
	public final native void setTriggerChangeCombined(boolean combined) /*-{
		this.triggerChangeCombined = combined;
	}-*/;

	@Override
	public final native void setEnableCurrentTarget(boolean enabled) /*-{
		this.enableCurrentTarget = enabled;
	}-*/;
}
