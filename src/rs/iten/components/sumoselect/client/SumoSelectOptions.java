/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.sumoselect.client;

/**
 * Additional (GWT specific) options that can be set for SumoSelect component.
 * 
 * @author Marko Nikolić
 *
 */
public interface SumoSelectOptions extends NativeSumoSelectOptions {
	/**
	 * Returns native options
	 */
	public NativeSumoSelectOptions getNativeOptions();
	
	/**
	 * Enables multiple selection if set.
	 * 
	 * @param multiple true to enable multiple selection, false otherwise
	 */
	public void setMultiple(boolean multiple);
	
	/**
	 * Return multiple option.
	 * 
	 * @return true if multiple is set, false otherwise
	 */
	public boolean getMultiple();
	
	/**
	 * Set name of the SumoSelect component.
	 * 
	 * @param name name of the component
	 */
	public void setName(String name);
	
	/**
	 * Return name of the component
	 * 
	 * @return name of the component
	 */
	public String getName();
}
