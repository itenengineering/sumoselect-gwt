/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.sumoselect.client;

/**
 * Native options that can be set for SumoSelect component.
 * 
 * @author Marko Nikolić
 *
 */
public interface NativeSumoSelectOptions {
	/**
	 * <p>
	 * The placeholder text to be displayed in the rendered select widget (on priority basis).
	 * </p>
	 * <p>
	 * Maximum priority is given to native placeholder attribute in select tag i.e. -
	 * <select placeholder="this is a placeholder" /> - Then the option with disabled and
	 * selected attribute i.e. <option disabled selected value="foo" > -
	 * Last to to the given placeholder attribute in the settings.
	 * <p>
	 * 
	 * @param placeholder placeholder text to be displayed in the rendered select widget
	 */
	public void setPlaceholder(String placeholder);
	
	/**
	 * @return placeholder text.
	 */
	public String getPlaceholder();

	/**
	 * The number of items to be displayed in the widget separated by a "," after that the text
	 * will be wraped as 3+ Selected. Set 0 for all the options.
	 * 
	 * @param count number of items to be displayed in the widget separated by a ","
	 */
	public void setCsvDispCount(int count);
	
	/**
	 * The format in which caption is displayed when more than csvDispCount items are selected.
	 * Its default value is '{0} Selected' ( {0} will be replaced by the seletion count ).
	 * 
	 * @param format format for caption
	 */
	public void setCaptionFormat(String format);
	
	/**
	 * Minimum screen width of device below which the options list is rendered in floating popup fashion.
	 * 
	 * @param width minimum screen width of device
	 */
	public void setFloatWidth(int width);
	
	/**
	 * Force the custom modal ( Floating list ) on all devices below floatWidth resolution.
	 * 
	 * @param force force the custom modal
	 */
	public void setForceCustomRendering(boolean force);
	
	/**
	 * The keywords to identify a mobile device from useragent string. The system default select
	 * list is rendered on the matched device.
	 * 
	 * @param useragents array of user agent strings
	 */
	public void setNativeOnDevice(String[] useragents);
	
	/**
	 * Output post data as csv of as default.
	 * 
	 * @param asCSV true to POST data as csv ( false for default select )
	 */
	public void setOutputAsCSV(boolean asCSV);
	
	/**
	 * @return true if data is POST as csv ( false for default select )
	 */
	public boolean getOutputAsCSV();
	
	/**
	 * Separation char if outputAsCSV is set to true.
	 * 
	 * @param separator separator char to use
	 */
	public void setCsvSepChar(String separator);
	
	/**
	 * Displays Ok Cancel buttons in desktop mode multiselect also.
	 * 
	 * @param okCancel true to display Ok Cancel buttons
	 */
	public void setOkCancelInMulti(boolean okCancel);
	
	/**
	 * In Multiselect mode whether to trigger change event on individual selection of each item
	 * or on combined selection ( pressing of OK or Cancel button ).
	 * 
	 * @param combined true for combined change event
	 */
	public void setTriggerChangeCombined(boolean combined);
	
	/**
	 * If enabled, change event will populate event.currentTarget. This is needed for GWT to work
	 * properly; otherwise change event will not fire on SumoSelect component.
	 * 
	 * @param enabled true to enable currentTarget, false to disable.
	 */
	public void setEnableCurrentTarget(boolean enabled);
}
