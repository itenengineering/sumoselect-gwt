/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.sumoselect.client;

/**
 * Class that has both native and gwt specific options.
 * 
 * @author Marko Nikolić
 *
 */
public class Options implements SumoSelectOptions {

	private boolean multiple = false;
	private String name;
	
	private NativeSumoSelectOptions nativeOptions = NativeOptions.create();

	/**
	 *<p>
	 * Options constructor.
	 * </p>
	 * <p>
	 * Each instance of Options class sets option enableCurrentTarget to true,
	 * because it is needed for GWT to function properly (changeEvent is not
	 * fired if this option is not set). If this is not desired functionality,
	 * this option must be explicitly set to false after object creation.
	 * </p
	 * <p>
	 * Example of a problem with jQuery can be found on <a href=http://jsbin.com/kirufetisu/1/edit?html,js,console,output>
	 * http://jsbin.com/kirufetisu/1/edit?html,js,console,output</a>
	 * </p> 
	 */
	public Options() {
		setEnableCurrentTarget(true);
	}
	
	@Override
	public NativeSumoSelectOptions getNativeOptions() {
		return nativeOptions;
	}

	@Override
	public void setPlaceholder(String placeholder) {
		nativeOptions.setPlaceholder(placeholder);
	}

	@Override
	public String getPlaceholder() {
		return nativeOptions.getPlaceholder();
	}
	
	@Override
	public void setCsvDispCount(int count) {
		nativeOptions.setCsvDispCount(count);
	}

	@Override
	public void setCaptionFormat(String format) {
		nativeOptions.setCaptionFormat(format);
	}

	@Override
	public void setFloatWidth(int width) {
		nativeOptions.setFloatWidth(width);
	}

	@Override
	public void setForceCustomRendering(boolean force) {
		nativeOptions.setForceCustomRendering(force);
	}

	@Override
	public void setNativeOnDevice(String[] useragents) {
		nativeOptions.setNativeOnDevice(useragents);
	}

	@Override
	public void setOutputAsCSV(boolean asCSV) {
		nativeOptions.setOutputAsCSV(asCSV);
	}

	@Override
	public boolean getOutputAsCSV() {
		return nativeOptions.getOutputAsCSV();
	}

	@Override
	public void setCsvSepChar(String separator) {
		nativeOptions.setCsvSepChar(separator);
	}

	@Override
	public void setOkCancelInMulti(boolean okCancel) {
		nativeOptions.setOkCancelInMulti(okCancel);
	}

	@Override
	public void setTriggerChangeCombined(boolean combined) {
		nativeOptions.setTriggerChangeCombined(combined);
	}

	@Override
	public void setEnableCurrentTarget(boolean enabled) {
		nativeOptions.setEnableCurrentTarget(enabled);
	}
	
	@Override
	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	@Override
	public boolean getMultiple() {
		return multiple;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}
